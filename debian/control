Source: trigger-rally
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Build-Depends: debhelper (>= 11),
 libopenal-dev,
 libalut-dev,
 libphysfs-dev,
 libsdl2-image-dev,
 docbook-to-man,
 libglew-dev,
 libtinyxml2-dev (>= 6.0.0)
Standards-Version: 4.4.0
Uploaders: Stefan Potyra <stefan@potyra.de>,
 Barry deFreese <bdefreese@debian.org>,
 Bertrand Marc <bmarc@debian.org>
Homepage: https://sourceforge.net/projects/trigger-rally
Vcs-Git: https://salsa.debian.org/games-team/trigger-rally.git
Vcs-Browser: https://salsa.debian.org/games-team/trigger-rally

Package: trigger-rally
Architecture: any
Depends: ${shlibs:Depends}, trigger-rally-data (= ${source:Version}), ${misc:Depends}
Description: 3D rally car racing game
 Trigger is a free 3D rally car racing game. Fun for all the family!
 .
 Trigger comes with a number of challenges where you have to race several
 tracks to finish each challenge.
 .
 When racing a track, you have to reach several locations marked by pulsating
 rings in sequence. To win a race you have to reach the last location in time.
 .
 Trigger is highly customisable, and it's easy to add new levels and vehicles.
 .
 An OpenGL accelerated video card is required to play Trigger.

Package: trigger-rally-data
Architecture: all
Depends: ${misc:Depends}
Recommends: trigger-rally (= ${binary:Version})
Replaces: trigger-really-data (<< 0.6.4~)
Breaks: trigger-really-data (<< 0.6.4~)
Description: 3D rally car racing game - data files
 Trigger is a free 3D rally car racing game. Fun for all the family!
 .
 Trigger comes with a number of challenges where you have to race several
 tracks to finish each challenge.
 .
 When racing a track, you have to reach several locations marked by pulsating
 rings in sequence. To win a race you have to reach the last location in time.
 .
 Trigger is highly customisable, and it's easy to add new levels and vehicles.
 .
 An OpenGL accelerated video card is required to play Trigger.
 .
 This package contains the data-files.
